<?php 

$lat = @$_GET['latitude'];
$lon = @$_GET['longitude'];

$secret = include_once 'secret.php';

// todo: add error handling!
if (!is_float((float)$lat) || !is_float((float)$lon)) {
	// header('HTTP/1.1 400 Latitude or Longitude must be float');
	// echo "Latitude or Longitude must be float";
	// die;
}

$url = "https://api.darksky.net/forecast/$secret/$lat,$lon";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
$dataFromAPI = curl_exec($ch);
curl_close($ch);

json_encode($dataFromAPI); 

