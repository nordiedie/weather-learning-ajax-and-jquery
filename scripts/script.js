App = {
	CurrentTown: 'Kolyubakino',
	Kolyubakino: {
		latitude: 55.6683495,
		longitude: 36.5127129
	},
	Kurgan: {
		latitude: 55.2627,
		longitude: 65.2028
	},
	Copenhagen: {
		latitude: 55.4113,
		longitude: 12.3500

	},
	ShowWeather: function () {
		$.ajax({
			url: "api/get-weather.php",
			data: {
				latitude: App[App.CurrentTown].latitude,
				longitude: App[App.CurrentTown].longitude
			},
			beforeSend: function () {
				$('.celsius').hide();
				$('.temp').html('Refreshing...');
			},
			success: function (dataFromServer) {
				let weather = JSON.parse(dataFromServer);
				let temperature = weather.currently.temperature;
				temperature = (temperature - 32) * (5/9);
				temperature = Math.round(temperature*100)/100;
				$('.temp').html(temperature);
				$('.celsius').show();
			}

		})
	}
};

$(document).ready(function () {
	$(".town").html(App.CurrentTown);
	App.ShowWeather();
	setInterval(App.ShowWeather, 30000);

});

$('.refresh').on('click', function () {
	App.ShowWeather();
});

$('.btn-town').on('click', function () {
	App.CurrentTown = this.innerText;
	$(".town").html(App.CurrentTown);
	App.ShowWeather();
});